from requests.auth import HTTPBasicAuth
from pprint import pprint
import requests
import time
import os


time.sleep(5)
domain = 'sonarcloud.io'
project = os.environ['CI_PROJECT_NAME']
branch = os.environ['CI_COMMIT_REF_NAME']
url = 'https://{}/api/qualitygates/project_status?projectKey={}&branch={}'
auth = HTTPBasicAuth(os.environ['K8S_SECRET_SONAR_LOGIN'], '')
req = requests.get(url.format(domain, project, branch), auth=auth)
pprint(req.json())
resp = req.json()
exit(resp['projectStatus']['status'] != 'OK')
