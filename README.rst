flisolcounter
=============

A simple conter to used on FLISoL events

Installation
------------
::

        pip install sanic

Testing
-------
::

        pytest --cov=flisolcounter --cov-report=xml --junitxml=pytest-report.xml

Running
-------
::

        python flisolcounter/app.py
