from sanic import Sanic
from sanic.response import text


app = Sanic()


@app.route("/", methods=['GET'])
async def hello(request):
    return text("Hello World!")


@app.route("/greetings", methods=['GET'])
async def hello(request):
    return text("Hello Human!")


@app.route("/greetings/<name>", methods=['GET'])
async def hello(request, name):
    return text("Hello {}!".format(name))

if __name__ == "__main__":  # pragma: no cover
    app.run(host="0.0.0.0", port=8000)
